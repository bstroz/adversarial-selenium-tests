package com.qa.examples.adversarial;
import com.qa.examples.adversarial.*;

import org.openqa.selenium.WebElement;

public class OverloadTest extends SimpleTest {
    private int getFieldLength(WebElement Field) {
        int lengthOfField = 0;
        String length = Field.getAttribute("maxlength");

        if (length == null) {
            length = "0";
        }
        lengthOfField = Integer.parseInt(length);
        
        return lengthOfField;
    }

    public void overloadField(WebElement Field) {
        int lengthOfData = 0;
        int fieldLength = getFieldLength(Field);

        if (fieldLength != 0) {
            lengthOfData = fieldLength * 20;
        } else {
            lengthOfData = 1024;
        }

        String fieldData = getOverloadData(lengthOfData);

        Field.sendKeys(fieldData);
        Field.submit();
    }

    public String getOverloadData(int n)
    {
  
        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz";
  
        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);
  
        for (int i = 0; i < n; i++) {
  
            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                = (int)(AlphaNumericString.length()
                        * Math.random());
  
            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                          .charAt(index));
        }
  
        return sb.toString();
    }
}
