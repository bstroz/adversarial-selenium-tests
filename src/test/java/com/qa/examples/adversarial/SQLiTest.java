package com.qa.examples.adversarial;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;


public class SQLiTest extends SimpleTest {

    public String simplePayload = "' or ''-'";
    
    public void insertSQLi(WebElement Field) {
        Field.sendKeys(simplePayload);
        Field.submit();
    }

    public void validateSQLi() {
        WebElement validation = wait.until(presenceOfElementLocated(By.className("validation-summary-errors")));

        try {
            assertTrue("No SQLi", (validation != null));
        } finally {
            assertTrue("No validation",(validation.getText() == "nevermatch"));
        }
    }
}
