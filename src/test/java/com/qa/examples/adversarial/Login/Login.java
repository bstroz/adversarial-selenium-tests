package com.qa.examples.adversarial.Login;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.qa.examples.adversarial.*;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class Login extends SimpleTest {
    public XSSTest xssEntity = new XSSTest();
    public SQLiTest sqliEntity = new SQLiTest();
    public static String loginUrl = "http://localhost:8000/Account/Login";

    @BeforeClass
    public static void BeforeTests() {
        //Navigate to the login url
        driver.get(loginUrl);
    }

    @Test
    public void UsernameSQLiTest() {
        // Wait for response text box to appear
        WebElement usernameField = wait.until(presenceOfElementLocated(By.id("Username")));
        WebElement passwordField = wait.until(presenceOfElementLocated(By.id("Password")));
        passwordField.sendKeys("password");
        
        sqliEntity.insertSQLi(usernameField);
        sqliEntity.validateSQLi();
    }
}