package com.qa.examples.adversarial;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class XSSTest extends SimpleTest {

    public String payloadText = "success";
    public String simplePayload = "<script>alert('"+payloadText+"');</script>";
    
    public void insertStoredXss(WebElement Field) {
        Field.sendKeys(simplePayload);
        Field.submit();
    }

    public void validateStoredXss() {
        // Catch the alert box
        Alert a = wait.until(ExpectedConditions.alertIsPresent());
        
        if (a!=null) {
            String alertText;
            alertText = a.getText();

            driver.switchTo().alert().accept();

            // We don't want to see a stored XSS, so we set this to fail
            assertTrue("There was an alert", (alertText == "nevermatch"));
        } else {
            // No alert box, so XSS was prevented
            assertTrue("XSS prevented!", a==null);
        }
    }
}
