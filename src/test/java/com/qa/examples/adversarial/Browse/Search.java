package com.qa.examples.adversarial.Browse;
import com.qa.examples.adversarial.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class Search extends SimpleTest {
    public XSSTest xssEntity = new XSSTest();
    public SQLiTest sqliEntity = new SQLiTest();
    public OverloadTest overloadEntity = new OverloadTest();
    public static String searchUrl = "http://localhost:8000/Product/Search";

    @BeforeClass
    public static void BeforeTests() {
        //Navigate to the login url
        driver.get(searchUrl);
    }

    @Test
    public void overloadSearch() {
        // Wait for response text box to appear
        WebElement searchBox = wait.until(presenceOfElementLocated(By.id("NameFilter")));

        overloadEntity.overloadField(searchBox);
    }
}
