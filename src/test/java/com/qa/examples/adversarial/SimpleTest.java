package com.qa.examples.adversarial;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Test;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import java.time.Duration;


public class SimpleTest {
    public static WebDriver driver;
    public static WebDriverWait wait;

    @BeforeClass
    public static void setupDriver() {
        System.setProperty("webdriver.gecko.driver","resources/drivers/geckodriver");

        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        
    }

    @Test
    public void loadPage() {
        try {
            driver.get("http://localhost:8000");
            
            WebElement firstResult = wait.until(presenceOfElementLocated(By.cssSelector("h2")));
            //System.out.println(firstResult.getAttribute("textContent"));
            String h2Result = firstResult.getAttribute("textContent");

            Assert.assertEquals("Welcome to the WebGoat.NetCore Store", h2Result.trim());
        } finally {
        }
    }

    @AfterClass
    public static void tearDownDriver() {
        driver.quit();
    }
}
  