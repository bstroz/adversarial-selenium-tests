package com.qa.examples.adversarial.Blog;

import org.junit.BeforeClass;
import org.junit.Test;
import com.qa.examples.adversarial.*;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class Blog extends SimpleTest {

    public XSSTest xssEntity = new XSSTest();
    public static String blogUrl = "http://localhost:8000/Blog/Index";

    @BeforeClass
    public static void BeforeTests() {
        // Navigate to the blog url
        driver.get(blogUrl);
    }

    @Test
    public void BlogStoredXssTest() {
        // Find reply button and click
        WebElement respondLink = driver.findElement(By.className("blogRespondButton"));
        respondLink.click();

        // Wait for response text box to appear
        WebElement textBox = wait.until(presenceOfElementLocated(By.className("BlogEntryReplyText")));

        // Insert payload and submit
        xssEntity.insertStoredXss(textBox);

        // Check whether there was an alert
        xssEntity.validateStoredXss();
    }
}
