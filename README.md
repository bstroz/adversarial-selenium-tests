# Adversarial Selenium Tests

## Purpose
A set of simple Selenium tests to demonstrate how one can use an adversarial mindset while performing QA testing of a web application.

## Setup
Though the tests are simply executed, they are designed to be run against the OWASP WebGoat.NET vulnerable web application.

### Prerequisites
To run these tests and the environment below, the following applications will need to be installed on the workstation:
```
docker
maven
Java
```

### WebGoat.NET
The simplest way to get a running version of WebGoat.NET is to use the pre-packaged Docker version from this Github repository:
https://github.com/tobyash86/WebGoat.NET

Follow the directions below to run the WebGoat.NET docker container locally on port 8000.

**docker build**
```
docker build --pull --rm -t webgoat.net .
```

**docker run**
```
docker run --rm -d -p 5000:80 --name webgoat.net webgoat.net
```

Open your browser and navigate to http://localhost:5000 to see WebGoat.NET

## Running the tests
Each test can be run individually using the command
```
mvn test -Dtest=Blog
```

Or the entire test suite can be run with the command
```
mvn test
```

## Tests included
### XSSTest
Injects a simple XSS payload into a given field.

### SQLiTest
Injects a simple SQL injection payload into a given field.

### OverloadTest
Creates random data of an overly large size and injects into a given field.